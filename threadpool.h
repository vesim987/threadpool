#include <vector>
#include <queue>
#include <thread>
#include <mutex>
#include <future>

class ThreadPool {
public:
    ThreadPool(size_t threads);
    ~ThreadPool();

    template<typename F, typename... Args,
             typename returnType = typename std::result_of<F(Args...)>::type>
    auto enqueue(F&& f, Args&&... args)->std::future<returnType>;

private:
    std::vector<std::thread> workers;

    std::queue < std::function<void()>> tasks;
    std::mutex queueMutex;

    bool isThreadPoolAborted;
};


ThreadPool::ThreadPool(size_t threads = std::thread::hardware_concurrency())
    : isThreadPoolAborted(false)
{

    if (threads == 0) {
        throw std::invalid_argument("number of threads should be greater than 0");
    }

    if (threads > std::thread::hardware_concurrency()) {
        throw std::invalid_argument("number of threads greater than number of cpu threads");
    }

    for (auto i = 0; i < threads; ++i) {
        workers.emplace_back(
        [this] {
            for (;;)
            {
                std::function<void()> task;

                {
                    std::lock_guard<std::mutex> lock(this->queueMutex);

                    if (this->tasks.empty() && !this->isThreadPoolAborted) {
                        std::this_thread::yield();
                        continue;
                    } else if (!this->tasks.empty()) {
                        task = std::move(this->tasks.front());
                        this->tasks.pop();
                    } else {
                        return;
                    }
                }

                task();
            }
        });
    }
}

ThreadPool::~ThreadPool()
{
    {
        std::lock_guard<std::mutex> lock(queueMutex);
        isThreadPoolAborted = true;
    }

    for (auto& worker : this->workers) {
        worker.join();
    }
}


template<typename F, typename... Args,
         typename returnType = typename std::result_of<F(Args...)>::type>
auto ThreadPool::enqueue(F&& f, Args&&... args) -> std::future<returnType> {

    auto task = std::make_shared<std::packaged_task<returnType()>>(
        std::bind(std::forward<F>(f), std::forward<Args>(args)...));

    std::future<returnType> res = task->get_future();

    {
        std::lock_guard<std::mutex> lock(this->queueMutex);

        if (this->isThreadPoolAborted)
        {
            throw std::runtime_error("enqueuing task in aborted thread pool");
        }

        tasks.emplace([task]()
        {
            (*task)();
        });

    }
    return res;
}