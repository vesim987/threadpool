#include <iostream>
#include "threadpool.h"

int main()
{
    ThreadPool threadPool;

    std::vector<std::future<int>> results;
    for (int i = 0; i < 10; ++i) {
        results.emplace_back(threadPool.enqueue(
        [](int a) {
            return a * 10;
        }, i));
    }

    for (auto& result : results) {
        std::cout << result.get() << std::endl;
    }

    return 0;
}

